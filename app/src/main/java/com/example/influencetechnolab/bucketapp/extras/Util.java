package com.example.influencetechnolab.bucketapp.extras;

import android.view.View;

import java.util.List;
/**
 * Created by Influence Technolab on 1/16/2017.
 */
public class Util
{
    public static void showViews(List<View> views)
    {
        for (View view:views)
        {
           view.setVisibility(view.VISIBLE);
        }
    }
    public static void hideViews(List<View> views)
    {
        for (View view:views)
        {
            view.setVisibility(view.GONE);
        }
    }
}
