package com.example.influencetechnolab.bucketapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.influencetechnolab.bucketapp.beans.Drop;
import com.example.influencetechnolab.bucketapp.beans.MarkListener;
import com.example.influencetechnolab.bucketapp.beans.SwipeListener;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;


/**
 * Created by Influence Technolab on 1/16/2017.
 */
public class AdapterDrops extends RecyclerView.Adapter<RecyclerView.ViewHolder>implements SwipeListener
{
    public static final int ITEM = 0;
    public static final int FOOTER = 1;
    LayoutInflater mInflater;
    Realm mrealm;
    MarkListener mMarkListener;
    AddListener mAddListener;
    RealmResults<Drop> mresults;
    public AdapterDrops(Context context,Realm realm ,RealmResults<Drop> result,MarkListener markListener)
    {
      mInflater = LayoutInflater.from(context);
        mrealm = realm;
        mMarkListener = markListener;
        update(result);
    }
    public void setAddListener(AddListener addListener)
    {
        mAddListener = addListener;
    }
    @Override
    public int getItemViewType(int position)
    {
        super.getItemViewType(position);
        if (mresults==null||position<mresults.size())
        {
            return ITEM;
        }
        else
        {
           return FOOTER;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        if (viewType == FOOTER)
        {
            View itemView = mInflater.from(parent.getContext())
                    .inflate(R.layout.footer, parent, false);
            return new FooterHolder(itemView);
        }
        else
        {
            View itemView = mInflater.from(parent.getContext())
                    .inflate(R.layout.raw_drop, parent, false);
            return new DropHolder(itemView,mMarkListener);
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
       if (holder instanceof DropHolder)
       {
           DropHolder dropholder = (DropHolder) holder;
           Drop drop = mresults.get(position);
           dropholder.mTextwhat.setText(drop.getWhat());
           dropholder.setWhen(drop.getWhen());
       }
    }

    @Override
    public int getItemCount()
    {
       /* if (mresults==null||mresults.isEmpty())
        {
            return 0;
        }

       else*/

            return mresults.size()+1;

    }
   /* public static ArrayList<String> generateValues()
    {
        ArrayList<String> dummyValues = new ArrayList<String>();
        for (int i=1;i<101;i++)
        {
           dummyValues.add("items"+i);
        }
        return dummyValues;
    }*/
public void update(RealmResults<Drop> realmResults)
{
    mresults = realmResults;
    notifyDataSetChanged();
}

    @Override
    public void onSwipe(int position)
    {
      if (position<mresults.size())
      {
          mrealm.beginTransaction();
          mresults.get(position).removeFromRealm();
          mrealm.commitTransaction();
          notifyItemRemoved(position);
      }
    }

    public void markComplete(int position)
    {
        if (position<mresults.size())
        {
            mrealm.beginTransaction();
            mresults.get(position).setCompleted(true);
            mrealm.commitTransaction();
            notifyItemRemoved(position);
        }
    }

    @Override
    public long getItemId(int position)
    {
        if (position<mresults.size())
        {
          return mresults.get(position).getAdded();
        }
        return RecyclerView.NO_ID;
    }

    public class DropHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
      TextView mTextwhat,mTextwhen;
        MarkListener mMarklistener;
        public DropHolder(View itemView,MarkListener markListener)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            mTextwhat = (TextView) itemView.findViewById(R.id.rtxt);
            mTextwhen = (TextView) itemView.findViewById(R.id.rtxt1);
            mMarklistener = markListener;
        }
        public void setWhen(long when)
        {
            String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
           //  mTextwhen.setText(when+"");
            mTextwhen.setText(DateUtils.getRelativeTimeSpanString(when,System.currentTimeMillis(),DateUtils.DAY_IN_MILLIS,0
            ));
          //  mTextwhen.setText(currentDateTimeString);
        }

        @Override
        public void onClick(View v)
        {
          mMarkListener.onMark(getAdapterPosition());
        }
    }
    public class FooterHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        Button footer;
        public FooterHolder(View itemView)
        {
            super(itemView);
            footer = (Button) itemView.findViewById(R.id.bttn_footer);
            footer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
          mAddListener.add();
        }
    }
}
