package com.example.influencetechnolab.bucketapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.influencetechnolab.bucketapp.beans.CompleteListener;
import com.example.influencetechnolab.bucketapp.beans.Drop;
import com.example.influencetechnolab.bucketapp.beans.Filter;
import com.example.influencetechnolab.bucketapp.beans.MarkListener;
import com.example.influencetechnolab.bucketapp.beans.SimpletouchCallback;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity
{
   // BucketRecyclerView bucketRecyclerView;
    BucketRecyclerView recyclerView;
    Toolbar mtoolbar;
    Realm realm;
    RealmResults<Drop> results;
    RealmChangeListener realmChangeListener;
    AdapterDrops adapterDrops;
    MarkListener markListener = new MarkListener()
    {
        @Override
        public void onMark(int position)
        {
          showMarkDialogue(position);
        }
    };
    AddListener addListener = new AddListener()
    {
        @Override
        public void add()
        {
           showDialogue();
        }
    };
    View eMptyView;
    CompleteListener completeListener = new CompleteListener()
    {
        @Override
        public void onComplete(int position)
        {
           // Toast.makeText(MainActivity.this,"Position in Activity " + position,Toast.LENGTH_SHORT ).show();
            adapterDrops.markComplete(position);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mtoolbar = (Toolbar)findViewById(R.id.toolbar);
      //  eMptyView = findViewById(R.id.empty_drops);

        recyclerView = (BucketRecyclerView) findViewById(R.id.recycle_view);
        realmChangeListener = new RealmChangeListener()
        {
            @Override
            public void onChange()
            {
                Log.d("msg","onChng() called");
                adapterDrops.update(results);
            }
        };
        RealmConfiguration configuration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(configuration);
        realm = Realm.getDefaultInstance();
        int filteropsn = load();
        loadResults(filteropsn);
         results = realm.where(Drop.class).findAllAsync();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setLayoutManager(layoutManager);
        adapterDrops = new AdapterDrops(this,realm,results,markListener);
        adapterDrops.setAddListener(addListener);
        adapterDrops.setHasStableIds(true);
        recyclerView.setAdapter(adapterDrops);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.stopScroll();
        SimpletouchCallback callback = new SimpletouchCallback(adapterDrops);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);
        /*bucketRecyclerView = (BucketRecyclerView)findViewById(R.id.recycler_view);
        bucketRecyclerView.hideifEmpty(mtoolbar);
        bucketRecyclerView.showifEmpty(eMptyView);
        bucketRecyclerView.setAdapter(adapterDrops);*/

        setSupportActionBar(mtoolbar);
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(this,MyIntentService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this,100,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,10,5,pendingIntent);
    }
    public void showDialogue()
    {
        DialogAdd dialogBox = new DialogAdd();
        dialogBox.show(getSupportFragmentManager(),"Add");
    }

    public void showMarkDialogue(int position)
    {
        DialogMark dialogMark = new DialogMark();
        Bundle bundle = new Bundle();
        bundle.putInt("POSITION",position);
        dialogMark.setArguments(bundle);
        dialogMark.setCompleteListener(completeListener);
        dialogMark.show(getSupportFragmentManager(),"Mark");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        boolean handled = true;
        int filterOption = Filter.NONE;
        switch (id) {
            case R.id.action_add:
                showDialogue();
                break;
            case R.id.sorting1:
                filterOption = Filter.LEAST_TIME_LEFT;
                save(Filter.LEAST_TIME_LEFT);
                break;
            case R.id.sorting2:
                filterOption = Filter.MOST_TIME_LEFT;
                save(Filter.MOST_TIME_LEFT);
                break;
            case R.id.sorting3:
                filterOption = Filter.COMPLETE;
                save(Filter.COMPLETE);
                break;
            case R.id.sorting4:
                filterOption = Filter.INCOMPLETE;
                save(Filter.INCOMPLETE);
                break;
            default:
                handled = false;
                break;
        }
        loadResults(filterOption);
        return handled;
    }

    public void save(int filterOption)
    {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("filter",filterOption);
        editor.apply();
    }
    public int load()
    {
       SharedPreferences pref = getPreferences(MODE_PRIVATE);
        int filterOpsn = pref.getInt("filter",Filter.NONE);
        return filterOpsn;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        results.addChangeListener(realmChangeListener);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        results.removeChangeListener(realmChangeListener);
    }
    public void loadResults(int filterOpsn)
    {
       switch (filterOpsn)
       {
           case Filter.NONE:
               results = realm.where(Drop.class).findAllAsync();
               break;
           case Filter.LEAST_TIME_LEFT:
               results = realm.where(Drop.class).findAllSortedAsync("when");
               break;
           case Filter.MOST_TIME_LEFT:
               results = realm.where(Drop.class).findAllSortedAsync("when", Sort.DESCENDING);
               break;
           case Filter.COMPLETE:
               results = realm.where(Drop.class).equalTo("completed",true).findAllAsync();
               break;
           case Filter.INCOMPLETE:
               results = realm.where(Drop.class).equalTo("completed",false).findAllAsync();
               break;
       }
        results.addChangeListener(realmChangeListener);
    }
}
