package com.example.influencetechnolab.bucketapp.beans;

/**
 * Created by Infl_1008 on 1/19/2017.
 */
public interface MarkListener
{
    void onMark(int position);
}
