package com.example.influencetechnolab.bucketapp.beans;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Infl_1008 on 1/23/2017.
 */
public class AppBucketDrops extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();
        RealmConfiguration configuration = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(configuration);
    }
}
