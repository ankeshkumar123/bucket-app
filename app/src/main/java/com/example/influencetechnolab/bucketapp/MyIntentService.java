package com.example.influencetechnolab.bucketapp;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.util.Log;

import com.example.influencetechnolab.bucketapp.beans.Drop;

import br.com.goncalves.pugnotification.notification.PugNotification;
import io.realm.Realm;
import io.realm.RealmResults;

import static android.R.id.message;
import static com.example.influencetechnolab.bucketapp.R.attr.title;


public class MyIntentService extends IntentService
{
    Drop current;

    public MyIntentService()
    {
        super("MyIntentService");
        Log.d("msg","MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Realm realm = null;
        try
        {
          realm = Realm.getDefaultInstance();
            RealmResults<Drop> realmResults = realm.where(Drop.class).equalTo("completed",false).findAll();
            fireNotification(current);
            for ( Drop current:realmResults)
            {
                if (isNotificationAdded(current.getAdded(),current.getWhen()))
                {

                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        Log.d("msg","onHandleIntent");
    }

    private void fireNotification(Drop drop)
    {
        PugNotification.with(this)
                .load()
                .title("Achievement")
                .message("good")
                .bigTextStyle("Congratulations, You are on the verge of accomplishing your goal")
                .smallIcon(R.drawable.pugnotification_ic_launcher)
                .largeIcon(R.drawable.pugnotification_ic_launcher)
                .flags(Notification.DEFAULT_ALL)
                .autoCancel(true)
                .click(MainActivity.class)
                .simple()
                .build();
    }

    private boolean isNotificationAdded(long added, long when)
    {
        long now = System.currentTimeMillis();
        if (now>when)
        {
            return false;
        }
        else
        {
            long dffrnce90 = (long) (0.9*(when - added));
            return (now>(added+dffrnce90)) ? true : false;
        }
    }


}
