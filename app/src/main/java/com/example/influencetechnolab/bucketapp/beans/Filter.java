package com.example.influencetechnolab.bucketapp.beans;

/**
 * Created by Infl_1008 on 1/20/2017.
 */
public interface Filter
{
    int NONE = 0;
    int MOST_TIME_LEFT = 1;
    int LEAST_TIME_LEFT = 2;
    int COMPLETE = 3;
    int INCOMPLETE = 4;
}
