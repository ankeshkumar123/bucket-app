package com.example.influencetechnolab.bucketapp;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.influencetechnolab.bucketapp.beans.CompleteListener;

/**
 * Created by Infl_1008 on 1/19/2017.
 */
public class DialogMark extends DialogFragment
{
    private ImageButton imageButton;
    private Button button;
    private View.OnClickListener mClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
          switch (v.getId())
          {
              case R.id.mark_bttn:
                  markAsComplete();
                  break;
          }
            dismiss();
        }
    };
    private CompleteListener mListener;

    private void markAsComplete()
    {
        Bundle arg = getArguments();
        if ( mListener!=null && arg!=null)
        {
            int post = arg.getInt("POSITION");
            mListener.onComplete(post);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.mark_dialogue,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        imageButton = (ImageButton) view.findViewById(R.id.close_bttn1);
        button = (Button) view.findViewById(R.id.mark_bttn);
        imageButton.setOnClickListener(mClickListener);
        button.setOnClickListener(mClickListener);
    }

    public void setCompleteListener(CompleteListener completeListener)
    {
        mListener = completeListener;
    }
}
