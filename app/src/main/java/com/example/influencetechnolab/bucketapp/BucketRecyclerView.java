package com.example.influencetechnolab.bucketapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.example.influencetechnolab.bucketapp.extras.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Influence Technolab on 1/16/2017.
 */
public class BucketRecyclerView extends RecyclerView
{
    private List<View> mNonEmptyViews = Collections.emptyList();
    private List<View> mEmptyViews = Collections.emptyList();
    private AdapterDataObserver mobserver = new AdapterDataObserver() {
        @Override
        public void onChanged()
        {
          toggleViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount)
        {
            toggleViews();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload)
        {
            toggleViews();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount)
        {
            toggleViews();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount)
        {
            toggleViews();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount)
        {
            toggleViews();
        }
    };
    public BucketRecyclerView(Context context)
    {
        super(context);
    }

    public BucketRecyclerView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public BucketRecyclerView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(Adapter adapter)
    {
        super.setAdapter(adapter);
        if (adapter!=null)
        {
            adapter.registerAdapterDataObserver(mobserver);
        }
        mobserver.onChanged();
    }

    public void hideifEmpty(View ... views)
    {
        mNonEmptyViews = Arrays.asList(views);
    }

    public void showifEmpty(View ... eMptyView)
    {
        mEmptyViews = Arrays.asList(eMptyView);
    }
    public void toggleViews()
    {
        if (getAdapter()!=null && !mEmptyViews.isEmpty() && !mNonEmptyViews.isEmpty())
        {
            if (getAdapter().getItemCount()==0)
            {
                Util.showViews(mEmptyViews);
             setVisibility(GONE);
                Util.hideViews(mNonEmptyViews);
            }
            else
            {
                Util.hideViews(mNonEmptyViews);
                setVisibility(VISIBLE);
                Util.showViews(mEmptyViews);
            }
        }
    }
}
