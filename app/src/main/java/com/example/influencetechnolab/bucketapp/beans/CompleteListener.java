package com.example.influencetechnolab.bucketapp.beans;

/**
 * Created by Infl_1008 on 1/20/2017.
 */
public interface CompleteListener
{
    public void onComplete(int position);
}
